package com.example.trackeractivity.Pertemuan6;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.trackeractivity.R;

public class SplashActivity extends AppCompatActivity {
    SharedPreferences session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Toast.makeText(SplashActivity.this, "SELAMAT DATANG", Toast.LENGTH_SHORT).show();
        session = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);
        if(!session.getString("nimnik", "").isEmpty() && !session.getString("nama", "").isEmpty()) {
            finish();
            startActivity(new Intent(SplashActivity.this, HomeActivity.class));
            return;
        }
        Thread thread = new Thread(){
            public void run(){
                try{
                    sleep(4000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }finally {
                    startActivity(new Intent(SplashActivity.this, PrefActivity.class));
                    finish();
                }
            }
        };
        thread.start();
    }
}