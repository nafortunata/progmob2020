package com.example.trackeractivity.Pertemuan6;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.trackeractivity.Crud.MainMhsActivity;
import com.example.trackeractivity.CrudDosen.MainDosenActivity;
import com.example.trackeractivity.CrudMatakuliah.MainMatkulActivity;
import com.example.trackeractivity.R;

public class HomeActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Button menuMhs = (Button)findViewById(R.id.buttonMahasiswa);
        Button menuDosen = (Button)findViewById(R.id.buttonDosen);
        Button menuMatkul = (Button)findViewById(R.id.buttonMatakuliah);
        Button btnLogout = (Button)findViewById(R.id.btnLogout);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(HomeActivity.this);
        if(sharedPreferences.getString("nimnik", "").isEmpty() && sharedPreferences.getString("nama", "").isEmpty()) {
            finish();
            startActivity(new Intent(HomeActivity.this, PrefActivity.class));
            return;
        }


        menuMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, MainMhsActivity.class);
                startActivity(i);
            }
        });

        menuDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, MainDosenActivity.class);
                startActivity(i);
            }
        });

        menuMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, MainMatkulActivity.class);
                startActivity(i);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.apply();
                finish();
                Intent i = new Intent(HomeActivity.this, PrefActivity.class);
                startActivity(i);

            }
        });
    }
}