package com.example.trackeractivity.CrudDosen;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.trackeractivity.Model.DefultResult;
import com.example.trackeractivity.Network.GetDataService;
import com.example.trackeractivity.Network.RetrofitClientInstance;
import com.example.trackeractivity.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UbahDosenActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_dosen);
        EditText edNidnLama = (EditText)findViewById(R.id.edNidnLama);
        EditText edNamaBaru = (EditText)findViewById(R.id.edNamaBaruDosen);
        EditText edNidnBaru = (EditText)findViewById(R.id.edNidnBaruDosen);
        EditText edAlamatBaru = (EditText)findViewById(R.id.edAlamatBaruDosen);
        EditText edEmailBaru = (EditText)findViewById(R.id.edEmailBaruDosen);
        EditText edGelarBaru = (EditText)findViewById(R.id.edGelarBaruDosen);
        Button btnSimpanUbah = (Button)findViewById(R.id.btnSimpanUbahDosen);
        pd = new ProgressDialog(UbahDosenActivity.this);

        btnSimpanUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefultResult> hapus = service.delete_dosen(
                        edNidnLama.getText().toString(),
                        "72180210");

                hapus.enqueue(new Callback<DefultResult>() {
                    @Override
                    public void onResponse(Call<DefultResult> call, Response<DefultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UbahDosenActivity.this,"BERHASIL DIHAPUS", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UbahDosenActivity.this,"GAGAL DIHAPUS", Toast.LENGTH_LONG).show();
                }
                });

                Call<DefultResult> call = service.add_dosen(
                        edNamaBaru.getText().toString(),
                        edNidnBaru.getText().toString(),
                        edAlamatBaru.getText().toString(),
                        edEmailBaru.getText().toString(),
                        edGelarBaru.getText().toString(),
                        "kosong",
                        "72180210"
                );

                call.enqueue(new Callback<DefultResult>() {
                    @Override
                    public void onResponse(Call<DefultResult> call, Response<DefultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UbahDosenActivity.this, "DATA BERHASIL DIUBAH", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UbahDosenActivity.this, "DATA GAGAL DIUBAH", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}