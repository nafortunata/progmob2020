package com.example.trackeractivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.trackeractivity.Pertemuan3.CardViewTestActivity;
import com.example.trackeractivity.Pertemuan3.ListActivity;
import com.example.trackeractivity.Pertemuan3.RecyclerActivity;
import com.example.trackeractivity.Pertemuan4.DebuggingActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //variable
        final TextView txtView = (TextView)findViewById(R.id.mainActivityTextView);
        Button myBtn = (Button)findViewById(R.id.button1);
        final EditText myEditTxt = (EditText)findViewById(R.id.editText1);
        Button btnHelp = (Button)findViewById(R.id.btnHelp);
        Button btnTrack = (Button)findViewById(R.id.btnTracker);
        //Pertemuan3
        Button btnList = (Button)findViewById(R.id.btnListView);
        Button btnRecycler = (Button)findViewById(R.id.btnRecyclerView);
        Button btnCard = (Button)findViewById(R.id.btnCardView);
        Button btnDebug = (Button)findViewById(R.id.btnDebugView);

        //action
        txtView.setText(R.string.text_hello_world);
        myBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.d("Klik Kuy",myEditTxt.getText().toString());
                txtView.setText(myEditTxt.getText().toString());

            }
        });
        btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HelpActivity.class);
                Bundle b = new Bundle();
                b.putString("help_string",myEditTxt.getText().toString());
                intent.putExtras(b);
                startActivity(intent);
            }
        });
        
        btnTrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, TrackerActivity.class);
                Bundle b = new Bundle();
                b.putString("text_string",myEditTxt.getText().toString());
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        //Pertemuan 3
        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                startActivity(intent);
            }
        });

        btnRecycler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RecyclerActivity.class);
                startActivity(intent);
            }
        });

        btnCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CardViewTestActivity.class);
                startActivity(intent);
            }
        });

        btnDebug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DebuggingActivity.class);
                startActivity(intent);
            }
        });

    }
}