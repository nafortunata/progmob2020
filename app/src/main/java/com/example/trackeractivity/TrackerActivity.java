package com.example.trackeractivity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class TrackerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracker);
        TextView txtTrack = (TextView)findViewById(R.id.textViewTrack);

        Bundle b = getIntent().getExtras();
        String textTrack = b.getString("text_string");
        txtTrack.setText(textTrack);
    }
}