package com.example.trackeractivity.CrudMatakuliah;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.trackeractivity.Model.DefultResult;
import com.example.trackeractivity.Network.GetDataService;
import com.example.trackeractivity.Network.RetrofitClientInstance;
import com.example.trackeractivity.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahMatkulActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_matkul);
        EditText NamaMatkul = (EditText)findViewById(R.id.txtNamaAddMatkul);
        EditText KodeMatkul = (EditText)findViewById(R.id.txtKodeAddMatkul);
        EditText HariMatkul = (EditText)findViewById(R.id.txtHariAddMatkul);
        EditText SesiMatkul = (EditText)findViewById(R.id.txtSesiAddMatkul);
        EditText SksMatkul = (EditText)findViewById(R.id.txtSksAddMatkul);
        Button btnTambahMkSimpan = (Button)findViewById(R.id.btnSimpanAddMatkul);
        pd = new ProgressDialog(TambahMatkulActivity.this);

        btnTambahMkSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Tunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefultResult> call = service.add_matkul(
                        NamaMatkul.getText().toString(),
                        "72180210",
                        KodeMatkul.getText().toString(),
                        Integer.parseInt(HariMatkul.getText().toString()),
                        Integer.parseInt(SesiMatkul.getText().toString()),
                        Integer.parseInt(SksMatkul.getText().toString())
                );
                call.enqueue(new Callback<DefultResult>() {
                    @Override
                    public void onResponse(Call<DefultResult> call, Response<DefultResult> response) {
                        pd.dismiss();
                        Toast.makeText(TambahMatkulActivity.this,"DATA BERHASIL DITAMBAHKAN", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(TambahMatkulActivity.this,"DATA GAGAL DITAMBAHKAN", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}