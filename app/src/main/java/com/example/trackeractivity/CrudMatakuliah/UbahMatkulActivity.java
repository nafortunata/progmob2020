package com.example.trackeractivity.CrudMatakuliah;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.trackeractivity.Model.DefultResult;
import com.example.trackeractivity.Network.GetDataService;
import com.example.trackeractivity.Network.RetrofitClientInstance;
import com.example.trackeractivity.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UbahMatkulActivity extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_matkul);
        EditText edKodeLama = (EditText)findViewById(R.id.edKodeLamaMatkul);
        EditText edNamaBaru = (EditText)findViewById(R.id.edNamaUbahMatkul);
        EditText edKodeBaru = (EditText)findViewById(R.id.edKodeBaruMatkul);
        EditText edHariBaru = (EditText)findViewById(R.id.edHariBaruMatkul);
        EditText edSesiBaru = (EditText)findViewById(R.id.edSesiBaruMatkul);
        EditText edSksBaru = (EditText)findViewById(R.id.edSksBaruMatkul);
        Button btnSimpanUbah = (Button)findViewById(R.id.btnSimpanUbahMatkul);
        pd = new ProgressDialog(UbahMatkulActivity.this);

        btnSimpanUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Tunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefultResult> hapus = service.delete_matkul(edKodeLama.getText().toString(),"72180210");
                hapus.enqueue(new Callback<DefultResult>() {
                    @Override
                    public void onResponse(Call<DefultResult> call, Response<DefultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UbahMatkulActivity.this,"BERHASIL DIHAPUS", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UbahMatkulActivity.this,"GAGAL DIHAPUS", Toast.LENGTH_LONG).show();
                    }
                });

                Call<DefultResult> call = service.add_matkul(
                        edNamaBaru.getText().toString(),
                        "72180210",
                        edKodeBaru.getText().toString(),
                        Integer.parseInt(edHariBaru.getText().toString()),
                        Integer.parseInt(edSesiBaru.getText().toString()),
                        Integer.parseInt(edSksBaru.getText().toString())
                );

                call.enqueue(new Callback<DefultResult>() {
                    @Override
                    public void onResponse(Call<DefultResult> call, Response<DefultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UbahMatkulActivity.this,"DATA BERHASIL DITAMBAHKAN", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UbahMatkulActivity.this,"DATA GAGAL DITAMBAHKAN", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
    }
}