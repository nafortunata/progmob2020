package com.example.trackeractivity.Crud;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.trackeractivity.Model.DefultResult;
import com.example.trackeractivity.Network.GetDataService;
import com.example.trackeractivity.Network.RetrofitClientInstance;
import com.example.trackeractivity.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateMhsActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_mhs);
        EditText editNimLama = (EditText)findViewById(R.id.editTxtNimLama);
        EditText editNamaBaru = (EditText)findViewById(R.id.editTxtNamaBaru);
        EditText editNimBaru = (EditText)findViewById(R.id.editTxtNimBaru);
        EditText editAlamatBaru = (EditText)findViewById(R.id.editTxtAlamatBaru);
        EditText editEmailBaru = (EditText)findViewById(R.id.editTxtEmailBaru);
        Button btnUpdate= (Button)findViewById(R.id.btnUpdateMhs);
        pd = new ProgressDialog(UpdateMhsActivity.this);

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefultResult> hapus = service.delete_mhs(editNimLama.getText().toString(),"72180210");
                hapus.enqueue(new Callback<DefultResult>() {
                    @Override
                    public void onResponse(Call<DefultResult> call, Response<DefultResult> response) {
                        Toast.makeText(UpdateMhsActivity.this,"BERHASIL DIHAPUS",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UpdateMhsActivity.this,"GAGAL DIHAPUS",Toast.LENGTH_LONG).show();
                    }
                });
                Call<DefultResult> call = service.add_mhs(
                        editNamaBaru.getText().toString(),
                        editNimBaru.getText().toString(),
                        editAlamatBaru.getText().toString(),
                        editEmailBaru.getText().toString(),
                        "kosongkan saja diisi bebas karena dirandom sistem",
                        "72180210"
                );
                call.enqueue(new Callback<DefultResult>() {
                    @Override
                    public void onResponse(Call<DefultResult> call, Response<DefultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UpdateMhsActivity.this, "DATA BERHASIL DIUBAH", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UpdateMhsActivity.this, "GAGAL DIUBAH", Toast.LENGTH_LONG).show();

                    }
                });
            }
        });

    }
}