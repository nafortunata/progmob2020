package com.example.trackeractivity.Crud;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.trackeractivity.Model.DefultResult;
import com.example.trackeractivity.Network.GetDataService;
import com.example.trackeractivity.Network.RetrofitClientInstance;
import com.example.trackeractivity.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaAddActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_add);

        final EditText editNama = (EditText)findViewById(R.id.editTxtNama);
        final EditText editNim = (EditText)findViewById(R.id.editTxtNim);
        final EditText editAlamat = (EditText)findViewById(R.id.editTxtAlamat);
        final EditText editEmail = (EditText)findViewById(R.id.editTxtEmail);
        Button btnSimpan = (Button)findViewById(R.id.btnSimpanMhs);
        pd = new ProgressDialog(MahasiswaAddActivity.this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefultResult> call = service.add_mhs(
                        editNama.getText().toString(),
                        editNim.getText().toString(),
                        editAlamat.getText().toString(),
                        editEmail.getText().toString(),
                        "kosongkan saja diisi bebas karena dirandom sistem",
                        "72180210"
                );

                call.enqueue(new Callback<DefultResult>() {
                    @Override
                    public void onResponse(Call<DefultResult> call, Response<DefultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaAddActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaAddActivity.this, "GAGAL", Toast.LENGTH_LONG).show();

                    }
                });
            }
        });

    }
}