package com.example.trackeractivity.Network;

import com.example.trackeractivity.Model.DefultResult;
import com.example.trackeractivity.Model.Dosen;
import com.example.trackeractivity.Model.Mahasiswa;
import com.example.trackeractivity.Model.Matakuliah;
import com.example.trackeractivity.Model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface GetDataService {
    //Mahasiswa
    @GET("api/progmob/mhs/{nim_progmob}")
    Call<List<Mahasiswa>> getMahasiswa(@Path("nim_progmob") String nim_progmob);

    @FormUrlEncoded
    @POST("api/progmob/mhs/delete")
    Call<DefultResult> delete_mhs(
            @Field("nim") String id,
            @Field("nim_progmob") String nim_progmob
    );
    @FormUrlEncoded
    @POST("api/progmob/mhs/create")
    Call<DefultResult> add_mhs(
            @Field("nama") String nama,
            @Field("nim") String nim,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob
    );
    @FormUrlEncoded
    @POST("api/progmob/mhs/update")
    Call<DefultResult>update_mhs(
            @Field("nama") String nama,
            @Field("nim") String nim,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob
    );

    //Dosen
    @GET("api/progmob/dosen/{nim_progmob}")
    Call<List<Dosen>> getDosen(@Path("nim_progmob") String nim_progmob);

    @FormUrlEncoded
    @POST("api/progmob/dosen/create")
    Call<DefultResult>add_dosen(
            @Field("nama") String nama,
            @Field("nidn") String nidn,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("gelar") String gelar,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob
    );

    @FormUrlEncoded
    @POST("api/progmob/dosen/delete")
    Call<DefultResult>delete_dosen(
            @Field("nidn") String id,
            @Field("nim_progmob") String nim_progmob
    );

    @FormUrlEncoded
    @POST("api/progmob/dosen/update")
    Call<DefultResult>update_dosen(
            @Field("nama") String nama,
            @Field("nidn") String nidn,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("gelar") String gelar,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob
    );

    //Matakuliah
    @GET("api/progmob/matkul/{nim_progmob}")
    Call<List<Matakuliah>> getMatakuliah(@Path("nim_progmob") String nim_progmob);

    @FormUrlEncoded
    @POST("api/progmob/matkul/create") //nama,nim_progmob,kode,hari,sesi,sks
    Call<DefultResult>add_matkul(
            @Field("nama") String nama,
            @Field("nim_progmob") String nim_progmob,
            @Field("kode") String kode,
            @Field("hari") int hari,
            @Field("sesi") int sesi,
            @Field("sks") int sks
    );

    @FormUrlEncoded
    @POST("api/progmob/matkul/update") //nama,nim_progmob,kode,hari,sesi,sks
    Call<DefultResult>update_matkul(
            @Field("nama") String nama,
            @Field("nim_progmob") String nim_progmob,
            @Field("kode") String kode,
            @Field("hari") int hari,
            @Field("sesi") int sesi,
            @Field("sks") int sks
    );

    @FormUrlEncoded
    @POST("api/progmob/matkul/delete") //nama,nim_progmob,kode,hari,sesi,sks
    Call<DefultResult>delete_matkul(
            @Field("kode") String id,
            @Field("nim_progmob") String nim_progmob
    );

    @FormUrlEncoded
    @POST("api/progmob/login")
    Call<List<User>>login(
            @Field("nimnik")String nimnik,
            @Field("password")String password
    );
}